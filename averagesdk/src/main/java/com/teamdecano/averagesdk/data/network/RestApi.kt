package com.teamdecano.averagesdk.data.network

import java.net.URL

/**
 * Created by Ren Decano on 2/20/18.
 */
class RestApi {

    private val ROKT_ENDPOINT = "https://roktcdn1.akamaized.net/store/test/android/prestored_scores.json"

    fun getPredefinedNumbers(): String = URL(ROKT_ENDPOINT).readText()
}