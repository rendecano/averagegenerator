package com.teamdecano.averagesdk.data.repository.source

/**
 * Created by Ren Decano on 2/20/18.
 */
class NumberLocalRepository {

    private val mListNumbers = arrayListOf<Double>()

    fun storePredefinedNumbersToCache(numberList: List<Double>) {
        mListNumbers.addAll(numberList)
    }

    fun storeNumberToCache(number: Double) {
        mListNumbers.add(number)
    }

    fun getAverage(): Double {
        val total: Double = mListNumbers.sum()

        return total / mListNumbers.size
    }
}