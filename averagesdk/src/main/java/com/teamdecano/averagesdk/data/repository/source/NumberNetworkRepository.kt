package com.teamdecano.averagesdk.data.repository.source

import com.teamdecano.averagesdk.data.network.RestApi

/**
 * Created by Ren Decano on 2/20/18.
 */
class NumberNetworkRepository(private val restApi: RestApi) {

    fun getPredefinedNumbersFromNetwork(): String = restApi.getPredefinedNumbers()
}