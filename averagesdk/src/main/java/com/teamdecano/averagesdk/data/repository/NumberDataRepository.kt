package com.teamdecano.averagesdk.data.repository

import com.teamdecano.averagesdk.data.repository.source.NumberLocalRepository
import com.teamdecano.averagesdk.data.repository.source.NumberNetworkRepository
import com.teamdecano.averagesdk.domain.repository.NumberRepository

/**
 * Created by Ren Decano on 2/20/18.
 */
class NumberDataRepository(val numberLocalRepository: NumberLocalRepository,
                           val numberNetworkRepository: NumberNetworkRepository) : NumberRepository {

    override fun getPredefinedNumbers(): String {
        return numberNetworkRepository.getPredefinedNumbersFromNetwork()
    }

    override fun storePredefinedNumbersList(numberList: List<Double>) {
        numberLocalRepository.storePredefinedNumbersToCache(numberList)
    }

    override fun storeNumber(number: Double) {
        numberLocalRepository.storeNumberToCache(number)
    }

    override fun getAverage(): Double {
        return numberLocalRepository.getAverage()
    }
}