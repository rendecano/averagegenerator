package com.teamdecano.averagesdk.domain.interactor

import com.teamdecano.averagesdk.domain.repository.NumberRepository

/**
 * Created by Ren Decano on 2/20/18.
 */
class AddNumberUseCase(val numberRepository: NumberRepository) {

    var numberToAdd : Double = 0.0

    fun execute() {
        numberRepository.storeNumber(numberToAdd)
    }

}