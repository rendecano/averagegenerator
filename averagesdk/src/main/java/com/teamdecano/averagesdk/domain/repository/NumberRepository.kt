package com.teamdecano.averagesdk.domain.repository

/**
 * Created by Ren Decano on 2/20/18.
 */
interface NumberRepository {

    fun getPredefinedNumbers(): String

    fun storePredefinedNumbersList(numberList: List<Double>)

    fun storeNumber(number: Double)

    fun getAverage(): Double
}