package com.teamdecano.averagesdk.domain.interactor

import com.teamdecano.averagesdk.domain.repository.NumberRepository

/**
 * Created by Ren Decano on 2/20/18.
 */
class GetAverageUseCase(val numberRepository: NumberRepository) {

    fun execute() : Double = numberRepository.getAverage()
}