package com.teamdecano.averagesdk.domain.interactor

import com.teamdecano.averagesdk.data.repository.NumberDataRepository
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.CoroutineScope
import kotlinx.coroutines.experimental.Deferred
import org.json.JSONArray

/**
 * Created by Ren Decano on 2/20/18.
 */
class InitializeUseCase(val numberDataRepository: NumberDataRepository) {

    suspend fun execute() {

        asyncAwait {
            val listNumbers = numberDataRepository.getPredefinedNumbers()

            val jsonArray = JSONArray(listNumbers)
            val listNumber = (0 until jsonArray.length()).map { jsonArray.getDouble(it) }
            numberDataRepository.storePredefinedNumbersList(listNumber)
        }
    }

    suspend fun <T> async(block: suspend CoroutineScope.() -> T): Deferred<T> {
        return kotlinx.coroutines.experimental.async(CommonPool) { block() }
    }

    suspend fun <T> asyncAwait(block: suspend CoroutineScope.() -> T): T {
        return async(block).await()
    }

}