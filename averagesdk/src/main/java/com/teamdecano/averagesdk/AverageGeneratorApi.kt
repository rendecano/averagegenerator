package com.teamdecano.averagesdk

import android.util.Log
import com.teamdecano.averagesdk.data.network.RestApi
import com.teamdecano.averagesdk.data.repository.NumberDataRepository
import com.teamdecano.averagesdk.data.repository.source.NumberLocalRepository
import com.teamdecano.averagesdk.data.repository.source.NumberNetworkRepository
import com.teamdecano.averagesdk.domain.interactor.AddNumberUseCase
import com.teamdecano.averagesdk.domain.interactor.GetAverageUseCase
import com.teamdecano.averagesdk.domain.interactor.InitializeUseCase
import kotlinx.coroutines.experimental.CoroutineScope
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch

/**
 * Created by Ren Decano on 2/20/18.
 */
class AverageGeneratorApi {

    private val TAG = javaClass.simpleName

    private var addNumberUseCase: AddNumberUseCase
    private var initializeUseCase: InitializeUseCase
    private var getAverageUseCase: GetAverageUseCase

    init {

        val restApi = RestApi()
        val numberNetworkRepository = NumberNetworkRepository(restApi)
        val numberLocalRepository = NumberLocalRepository()
        val numberRepository = NumberDataRepository(numberLocalRepository, numberNetworkRepository)

        initializeUseCase = InitializeUseCase(numberRepository)
        addNumberUseCase = AddNumberUseCase(numberRepository)
        getAverageUseCase = GetAverageUseCase(numberRepository)

        launchAsync {
            try {
                initializeUseCase.execute()
            } catch (exception: Exception) {
                Log.d(TAG, exception.message)
            }
        }
    }

    fun addNumber(number: Double) {
        addNumberUseCase.numberToAdd = number
        addNumberUseCase.execute()
    }

    fun getAverage(): Double {
        return getAverageUseCase.execute()
    }

    private fun launchAsync(block: suspend CoroutineScope.() -> Unit): Job {
        return launch(UI) { block() }
    }
}