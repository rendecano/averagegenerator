package com.teamdecano.averagegenerator

import com.teamdecano.averagesdk.AverageGeneratorApi

/**
 * Created by Ren Decano on 2/20/18.
 */
class GetAveragePresenter(private val view: GetAverageView) {

    var averageGeneratorApi: AverageGeneratorApi = AverageGeneratorApi()

    fun setNumber(number: Double) {
        averageGeneratorApi.addNumber(number)
    }

    fun getAverage(): Double = averageGeneratorApi.getAverage()

}