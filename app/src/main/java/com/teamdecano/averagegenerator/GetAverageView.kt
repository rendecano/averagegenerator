package com.teamdecano.averagegenerator

/**
 * Created by Ren Decano on 2/20/18.
 */
interface GetAverageView {

    fun showError(message: String)
}