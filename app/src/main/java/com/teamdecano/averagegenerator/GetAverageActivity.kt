package com.teamdecano.averagegenerator

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_get_average.*

class GetAverageActivity : AppCompatActivity(), GetAverageView {

    private val presenter: GetAveragePresenter = GetAveragePresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_get_average)

        saveButton.setOnClickListener {
            val input = numberText.text.toString()

            if (input.isNotEmpty()) {
                presenter.setNumber(input.toDouble())
                numberText.setText("")
            } else {
                Toast.makeText(this, getString(R.string.input_error), Toast.LENGTH_SHORT).show()
            }
        }

        getAverageButton.setOnClickListener {
            averageText.text = presenter.getAverage().toString()
        }
    }

    override fun showError(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}
