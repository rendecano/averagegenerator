# README #
AverageGenerator

### SDK

### Name: 
* AverageGeneratorApi

### Architecture used:
* Clean architecture

### Language: 
* Kotlin (with coroutines)

### Methods: 
* fun addNumber(number: Double)
* fun getAverage() :  Double

### Sample usage: 
var averageGeneratorApi: AverageGeneratorApi = AverageGeneratorApi()

averageGeneratorApi.addNumber(27)

val average = averageGeneratorApi.getAverage()

###  MAIN APPLICATION 

### Name: 
* AverageGenerator

### Architecture used: 
* MVP

### Language: 
* Kotlin

